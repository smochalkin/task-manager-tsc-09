package ru.smochalkin.tm.component;

import ru.smochalkin.tm.api.ICommandController;
import ru.smochalkin.tm.api.ICommandRepository;
import ru.smochalkin.tm.api.ICommandService;
import ru.smochalkin.tm.constant.ArgumentConst;
import ru.smochalkin.tm.constant.TerminalConst;
import ru.smochalkin.tm.controller.CommandController;
import ru.smochalkin.tm.repository.CommandRepository;
import ru.smochalkin.tm.service.CommandService;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void start(final String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        parseArgs(args);
        process();
    }

    public void parseArgs(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        commandController.exit();
    }

    public void parseArg(final String arg) {
        switch (arg) {
            case ArgumentConst.HELP: commandController.showHelp(); break;
            case ArgumentConst.ABOUT: commandController.showAbout(); break;
            case ArgumentConst.VERSION: commandController.showVersion(); break;
            case ArgumentConst.INFO: commandController.showInfo(); break;
            case ArgumentConst.COMMANDS: commandController.showCommands(); break;
            case ArgumentConst.ARGUMENTS: commandController.showArguments(); break;
            default: commandController.showErrorArgument();
        }
    }

    public void process() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!TerminalConst.EXIT.equals(command)) {
            System.out.print("Enter command: ");
            command = scanner.nextLine();
            parseCommand(command);
            System.out.println();
        }
    }

    public void parseCommand(final String command) {
        switch (command) {
            case TerminalConst.HELP: commandController.showHelp(); break;
            case TerminalConst.ABOUT: commandController.showAbout(); break;
            case TerminalConst.VERSION: commandController.showVersion(); break;
            case TerminalConst.INFO: commandController.showInfo(); break;
            case TerminalConst.COMMANDS: commandController.showCommands(); break;
            case TerminalConst.ARGUMENTS: commandController.showArguments(); break;
            case TerminalConst.EXIT: commandController.exit(); break;
            default: commandController.showErrorCommand();
        }
    }

}
