package ru.smochalkin.tm.service;

import ru.smochalkin.tm.api.ICommandRepository;
import ru.smochalkin.tm.api.ICommandService;
import ru.smochalkin.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getCommands() {
        return commandRepository.getCommands();
    }

}
