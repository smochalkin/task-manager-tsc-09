package ru.smochalkin.tm.api;

import ru.smochalkin.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
